function showForm() {
    $('.block-4 .item .butt').click(function (e) {

        var form = $(this).parents('.item').find('.form');


        $(this).stop().toggleClass('active');
        if($(this).hasClass('active')){
            form.stop().slideDown();
        }else{
            form.stop().slideUp();
        }
        e.preventDefault();

    });
}


$(document).ready(function(){
    showForm();
});

$(window).load(function(){

});

$(window).resize(function(){

});